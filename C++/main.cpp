#include "test_GeometryLibrary.hpp"
#include "test_MeshManager.hpp"
//#include "test_PolygonCutter.hpp"
#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
