#ifndef IMSHMNG_H
#define IMSHMNG_H
#include <vector>
#include "GeometryLibrary.hpp"

using namespace std;
using namespace GeometryLib;

namespace Mesh {

class IMeshManager {
public:
    virtual void CreateReferenceElement(const Polygon& poly, const Polygon& bB, ReferenceElement& rf) = 0;
    virtual void CreateMesh(const Polygon& domain, ReferenceElement& rf, vector<int>& mesh, const bool& mode_box) = 0;
};
}

#endif // IMSHMNG_H
