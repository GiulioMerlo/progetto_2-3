#ifndef REFELM_H
#define REFELM_H
#include <vector>

using namespace std;

namespace GeometryLib {

class ReferenceElement {
public:
    vector<int> Polygons;
    vector<int> BoundingBox;
};
}

#endif // REFELM_H
