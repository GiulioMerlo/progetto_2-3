#ifndef POLYCUT_H
#define POLYCUT_H
#include "IPolygonCutter.hpp"
#include "GeometryLibrary.hpp"

using namespace GeometryLib;
namespace PolyCut {

  class PolygonCutter: public IPolygonCutter {
  private:
      IGeometryLibrary& _geometryLib;
  public:
      PolygonCutter(IGeometryLibrary& geometryLib) : _geometryLib(geometryLib) {}
      vector<vector<int>> CutPolygon(const Polygon& polygon,
                                     const Segment& straight);
  };
}

#endif // POLYCUT_H
