#include "MeshManager.hpp"
#include <map>
using namespace GeometryLib;
namespace Mesh {

void Mesh::MeshManager::CreateReferenceElement(const Polygon &poly, const Polygon &bB, ReferenceElement &rf)
{
    Point point;
    Segment tmp_segment;
    double key;
    int id, start = 0, writing_on = 0, search;
    vector<map<double, pair<int, bool>>> S_E_N_W;
    vector<pair<int,bool>> ordered_points;
    vector<vector<int>> newPolygons;
    bool check;
    vector<int>new_poly;
    vector<int>tmp_poly;

    rf.Polygons.clear();
    rf.BoundingBox.clear();
    S_E_N_W.resize(4);

    for(unsigned int i = 0; i < S_E_N_W.size(); i++){
        S_E_N_W[i][0] = pair<int, bool>(bB.Vertices[i], false);
    }

    //start by ordering original polygon points
    for (unsigned int i = 0; i < poly.Vertices.size(); i++){
        point = _geometryLib.getPoint(poly.Vertices[i]);
        for (unsigned int j = 0; j < S_E_N_W.size(); j++){
            if (_geometryLib.isOnSegment(point, _geometryLib.getSegment(bB.Sides[j]))){
                tmp_segment.Start = bB.Vertices[j];
                tmp_segment.End = point.Id_point;
                key = _geometryLib.dotProduct(tmp_segment, tmp_segment);
                if(key == 0){
                    S_E_N_W[j][key].first = point.Id_point;
                    S_E_N_W[j][key].second = true;
                }
                else if(key != _geometryLib.dotProduct(_geometryLib.getSegment(bB.Sides[j]), _geometryLib.getSegment(bB.Sides[j]))){
                    S_E_N_W[j][key] = pair<int, bool>(point.Id_point, true);
                }
            }
        }
    }

    //add points for coherent mesh creation
    for(unsigned int i = 0; i < S_E_N_W.size(); i++){
        tmp_segment.Start = bB.Vertices[(i + 1) % S_E_N_W.size()];
        for (auto it = S_E_N_W[i].begin(); it != S_E_N_W[i].end(); it++){
            if(it -> second.first != S_E_N_W[i][0].first){
                tmp_segment.End = it -> second.first;
                key = _geometryLib.dotProduct(tmp_segment, tmp_segment);
                if(S_E_N_W[(i + 2) % S_E_N_W.size()].emplace(key, pair<int, bool>(-1, false)).second){
                    id = _geometryLib.numberPoints();
                    if(i % 2 == 0){
                        _geometryLib.newPoint(_geometryLib.getPoint(tmp_segment.End).X, _geometryLib.getPoint(bB.Vertices[(i + 2) % S_E_N_W.size()]).Y, id);
                    }
                    else {
                        _geometryLib.newPoint(_geometryLib.getPoint(bB.Vertices[(i + 2) % S_E_N_W.size()]).X, _geometryLib.getPoint(tmp_segment.End).Y, id);
                    }
                    S_E_N_W[(i + 2) % S_E_N_W.size()][key].first = id;
                }
            }
        }
    }

    //move all points int  a single vector
    ordered_points.reserve(poly.Vertices.size() + 8);
    for (unsigned int i = 0; i < S_E_N_W.size(); i++){
        for (auto it = S_E_N_W[i].begin(); it != S_E_N_W[i].end(); it++){
            ordered_points.push_back(it -> second);
        }
    }

    //prepare for dividing polygons
    while(!ordered_points[start].second){
        start++;
    }

    newPolygons.push_back(vector<int>());
    newPolygons[0].push_back(ordered_points[start].first);

    //create new polygons: step one
    for(unsigned int i = 1; i < ordered_points.size(); i++){
        newPolygons[writing_on].push_back(ordered_points[(i + start) % ordered_points.size()].first);
        if(ordered_points[(i + start) % ordered_points.size()].second){
            writing_on ++;
            newPolygons.push_back(vector<int>());
            newPolygons[writing_on].push_back(ordered_points[i + start].first);
        }
    }
    newPolygons[writing_on].push_back(ordered_points[start].first);

    //create new polygons: step 2
    for(unsigned int i = 0; i < newPolygons.size(); i++){
        search = 0;
        while(poly.Vertices[search] != newPolygons[i].back()){
            search ++;
        }
        if (search == 0){
            search = poly.Vertices.size();
        }
        while(poly.Vertices[(search - 1) % poly.Vertices.size()] != newPolygons[i][0]){
            newPolygons[i].push_back(poly.Vertices[(search - 1) % poly.Vertices.size()]);
            search--;
        }
    }

    new_poly = poly.Vertices;

    //add meaningful polygons to the library collection or update orignal polygon with new vertices
    for(unsigned int i = 0; i < newPolygons.size(); i++){
        if(newPolygons[i].size() >= 3){
            check = true;
            for (unsigned int j = 1; j < newPolygons[i].size() - 1; j++){
                if (!_geometryLib.parallelismCheck({-1, newPolygons[i][j-1], newPolygons[i][j],}, {-1, newPolygons[i][j], newPolygons[i][j+1]})){
                    check = false;
                    break;
                }
            }
            if (check){
                search = 0;
                while (poly.Vertices[search] != newPolygons[i].back()) {
                    search ++;
                }
                for(unsigned int j = 0; j < newPolygons[i].size(); j++){
                    tmp_poly.push_back(newPolygons[i][j]);
                }
                for(unsigned int j = 2; j < new_poly.size(); j++){
                    tmp_poly.push_back(new_poly[(start + j) % new_poly.size()]);
                }
                new_poly = tmp_poly;
                tmp_poly.clear();
            }
            else {
                id = _geometryLib.numberPolygons();
                _geometryLib.newPolygon(newPolygons[i], id);
                rf.Polygons.push_back(id);
            }
        }
    }
    id = _geometryLib.numberPolygons();
    _geometryLib.newPolygon(new_poly, id);
    rf.Polygons.push_back(id);

    //save bounding box
    for(unsigned int i = 0; i < bB.Vertices.size(); i++){
        rf.BoundingBox.push_back(bB.Vertices[i]);
    }

    rf.BoundingBox.shrink_to_fit();
    rf.Polygons.shrink_to_fit();
}

void MeshManager::CreateMesh(const Polygon &domain, ReferenceElement &rf, vector<int> &mesh, const bool& mode_box)
{
    mesh.clear();
    Polygon domainBB = _geometryLib.getPolygon(_geometryLib.computeBoundingBox(domain));
    double rf_width = _geometryLib.getPoint(rf.BoundingBox[1]).X - _geometryLib.getPoint(rf.BoundingBox[0]).X;
    double rf_height = _geometryLib.getPoint(rf.BoundingBox[3]).Y - _geometryLib.getPoint(rf.BoundingBox[0]).Y;
    double dom_width = _geometryLib.getPoint(domainBB.Vertices[1]).X - _geometryLib.getPoint(domainBB.Vertices[0]).X;
    double dom_height = _geometryLib.getPoint(domainBB.Vertices[3]).Y - _geometryLib.getPoint(domainBB.Vertices[0]).Y;
    double delta_x = _geometryLib.getPoint(domainBB.Vertices[0]).X - _geometryLib.getPoint(rf.BoundingBox[0]).X;
    double delta_y = _geometryLib.getPoint(domainBB.Vertices[0]).Y - _geometryLib.getPoint(rf.BoundingBox[0]).Y;
    int id;
    bool check;
    vector<int>bBmesh;
    bBmesh.clear();
    vector<vector<int>> cutPolygons;
    vector<Polygon> tmpPolygons;
    vector<int> cutrf;
    vector<int> cutrf_last_row;

    //place polygons that don't neeed cuts
    for (int i = 0; i < (int)(dom_height/rf_height); i++){
        for (int j = 0; j < (int)(dom_width/rf_width); j++){
            for (unsigned int k = 0; k < rf.Polygons.size(); k++){
                bBmesh.push_back(_geometryLib.traslate(_geometryLib.getPolygon(rf.Polygons[k]), delta_x + (j*rf_width), delta_y + (i * rf_height)));
            }
        }
    }

    //create a new reference element for the last column and start filling its base
    for(unsigned int k = 0; k < rf.Polygons.size(); k++){
        id = _geometryLib.traslate(_geometryLib.getPolygon(rf.Polygons[k]), delta_x + ((int)(dom_width/rf_width))*rf_width, delta_y);
        cutPolygons = _polyCutter.CutPolygon(_geometryLib.getPolygon(id), {-1, domainBB.Vertices[1], domainBB.Vertices[2]});
        for (unsigned int t = 0; t < cutPolygons.size(); t++){
            check = true;
            for (unsigned int s = 0; s < cutPolygons[t].size(); s++){
                if(!_geometryLib.pointInPolygon(domainBB, _geometryLib.getPoint(cutPolygons[t][s]))){
                    check = false;
                    break;
                }
            }
            if (check){
                id = _geometryLib.numberPolygons();
                _geometryLib.newPolygon(cutPolygons[t], id);
                cutrf.push_back(id);
                bBmesh.push_back(id);
            }
        }
    }

    //use cutrf to place polygons on last column
    for (int i = 1; i < (int)(dom_height/rf_height); i++){
        for(unsigned int k = 0; k < cutrf.size(); k++){
            bBmesh.push_back(_geometryLib.traslate(_geometryLib.getPolygon(cutrf[k]),0, (i * rf_height)));
        }
    }

    //create a new reference element for the last row and start filling the left edge
    for (unsigned int k = 0; k < rf.Polygons.size(); k++){
        id = _geometryLib.traslate(_geometryLib.getPolygon(rf.Polygons[k]), delta_x, delta_y + ((int)(dom_height/rf_height) * rf_height));
        cutPolygons = _polyCutter.CutPolygon(_geometryLib.getPolygon(id), {-1, domainBB.Vertices[2], domainBB.Vertices[3]});
        for (unsigned int t = 0; t < cutPolygons.size(); t++){
            check = true;
            for (unsigned int s = 0; s < cutPolygons[t].size(); s++){
                if(!_geometryLib.pointInPolygon(domainBB, _geometryLib.getPoint(cutPolygons[t][s]))){
                    check = false;
                    break;
                }
            }
            if (check){
                id = _geometryLib.numberPolygons();
                _geometryLib.newPolygon(cutPolygons[t], id);
                cutrf_last_row.push_back(id);
                bBmesh.push_back(id);
            }
        }
    }

    //use cutrf_last_row to place polygons on last row
    for (int j = 1; j < (int)(dom_width/rf_width); j++){
        for (unsigned int k = 0; k < cutrf_last_row.size(); k++){
            bBmesh.push_back(_geometryLib.traslate(_geometryLib.getPolygon(cutrf_last_row[k]), (j*rf_width), 0));
        }
    }

    //cut again cutrf and fill last corner
    for (unsigned int k = 0; k < cutrf.size(); k++){
        id = _geometryLib.traslate(_geometryLib.getPolygon(cutrf[k]), 0, ((int)(dom_height/rf_height) * rf_height));
        cutPolygons = _polyCutter.CutPolygon(_geometryLib.getPolygon(id), {-1, domainBB.Vertices[2], domainBB.Vertices[3]});
        for (unsigned int t = 0; t < cutPolygons.size(); t++){
            check = true;
            for (unsigned int s = 0; s < cutPolygons[t].size(); s++){
                if(!_geometryLib.pointInPolygon(domainBB, _geometryLib.getPoint(cutPolygons[t][s]))){
                    check = false;
                    break;
                }
            }
            if (check){
                id = _geometryLib.numberPolygons();
                _geometryLib.newPolygon(cutPolygons[t], id);
                bBmesh.push_back(id);
            }
        }
    }

    if(!mode_box){
        //PROJECT 3:
        Polygon poly;
        vector<Polygon> startPoly, tmpVector;
        bool oneinside;
        int insideCounter;

        for(unsigned int i = 0; i < bBmesh.size(); i++){
            poly = _geometryLib.getPolygon(bBmesh[i]);
            oneinside = false;
            insideCounter = 0;
            for (unsigned int j = 0; j < poly.Vertices.size(); j++){
                if(_geometryLib.pointInPolygon(domain, _geometryLib.getPoint(poly.Vertices[j]))){
                    oneinside = true;
                    insideCounter++;
                }
            }
            if (insideCounter == (int)poly.Vertices.size()){
                //the polygon is completly inside of the domain so it is added to the new mesh collection
                mesh.push_back(poly.Id_polyogon);
            }
            else if (oneinside){
                //the polygon is cutted and its subpolygons are divided accrding to the position with respect to the domain
                startPoly = {poly};
                tmpPolygons.clear();
                for (unsigned int j = 0; j < domain.Sides.size(); j++){
                    for (unsigned int k = 0; k < startPoly.size(); k++){
                        cutPolygons = _polyCutter.CutPolygon(startPoly[k], _geometryLib.getSegment(domain.Sides[j]));
                        for (unsigned int t = 0; t < cutPolygons.size(); t++){
                            check = true;
                            for (unsigned int s = 0; s < cutPolygons[t].size(); s++){
                                if (_geometryLib.crossProduct(_geometryLib.getSegment(domain.Sides[j]), {-1,_geometryLib.getSegment(domain.Sides[j]).Start, cutPolygons[t][s]}) < -1e-7){
                                    check = false;
                                    break;
                                }
                            }
                            if (check){
                                id = _geometryLib.numberPolygons();
                                _geometryLib.newPolygon(cutPolygons[t], id);
                                tmpPolygons.push_back(_geometryLib.getPolygon(id));
                            }
                        }
                    }
                    startPoly = tmpPolygons;
                    tmpPolygons.clear();
                }
                for (unsigned int j = 0; j < startPoly.size(); j++){
                    mesh.push_back(startPoly[j].Id_polyogon);
                }
            }
        }
    }
    else {
        mesh = bBmesh;
    }
}

}
