#ifndef POLYGON_H
#define POLYGON_H
#include <vector>

using namespace std;

namespace GeometryLib {

class Polygon {
public:
    int Id_polyogon;
    vector<int> Vertices;
    vector<int> Sides;
};
}

#endif // POLYGON_H
