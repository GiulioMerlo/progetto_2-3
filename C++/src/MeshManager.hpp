#ifndef MSHMNG_H
#define MSHMNG_H
#include "IMeshManager.hpp"
#include "GeometryLibrary.hpp"
#include "PolygonCutter.hpp"

using namespace GeometryLib;
using namespace PolyCut;
namespace Mesh {

  class MeshManager: public IMeshManager {
  private:
      IGeometryLibrary& _geometryLib;
      IPolygonCutter& _polyCutter;
  public:
      MeshManager(IGeometryLibrary& geometryLib, IPolygonCutter& polyCutter) : _geometryLib(geometryLib), _polyCutter(polyCutter) {}
      void CreateReferenceElement(const Polygon& poly, const Polygon& bB, ReferenceElement& rf);
      void CreateMesh(const Polygon& domain, ReferenceElement& rf, vector<int>& mesh, const bool& mode_box);
  };
}

#endif // MSHMNG_H
