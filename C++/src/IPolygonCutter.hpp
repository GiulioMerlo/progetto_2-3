#ifndef IPOLYCUT_H
#define IPOLYCUT_H
#include "GeometryLibrary.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace GeometryLib;

namespace PolyCut {

class IPolygonCutter {
public:
    virtual vector<vector<int>> CutPolygon(const Polygon& polygon,
                                           const Segment& straight) = 0;

};
}

#endif // IPOLYCUT_H
