#ifndef SEGMENT_H
#define SEGMENT_H

namespace GeometryLib {

class Segment {
public:
    int Id_segment;
    int Start;
    int End;
};
}

#endif // SEGMENT_H
