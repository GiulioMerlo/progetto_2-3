#ifndef __TEST_GEOMLIB_H
#define __TEST_GEOMLIB_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "GeometryLibrary.hpp"

using namespace GeometryLib;
using namespace testing;
using namespace std;

namespace GeometryTesting {

TEST(TestGeometryLib, TestComputeArea){
    GeometryLibrary g;
    g.reset();
    double Area;
    g.newPoint(1,1,0);
    g.newPoint(4,1,1);
    g.newPoint(4,4,2);
    g.newPoint(1,4,3);
    g.newPolygon({0, 1, 2, 3}, 0);
    Area = g.computeArea(g.getPolygon(0));

    EXPECT_EQ(Area, 9);

}
TEST(TestGeometryLib, TestPointInPolygon){
    GeometryLibrary g;
    g.reset();
    g.newPoint(0,0,0);
    g.newPoint(4,0,1);
    g.newPoint(4,4,2);
    g.newPoint(0,4,3);
    g.newPolygon({0, 1, 2, 3}, 0);

    g.newPoint(1, 3, 4);
    g.newPoint(-2, -2, 5);

    EXPECT_EQ(g.pointInPolygon(g.getPolygon(0), g.getPoint(4)), true);

    EXPECT_EQ(g.pointInPolygon(g.getPolygon(0), g.getPoint(5)), false);

    EXPECT_EQ(g.pointInPolygon(g.getPolygon(0), g.getPoint(3)), true);

}
TEST(TestGeometryLib, TestComputeBoundingBox){
    GeometryLibrary g;
    int id_bounding_box;
    Polygon bounding_box;
    g.reset();
    g.newPoint(4, 1, 0);
    g.newPoint(2, 3, 1);
    g.newPoint(0, 1, 2);
    g.newPolygon({0, 1, 2}, 0);

    id_bounding_box = g.computeBoundingBox(g.getPolygon(0));
    bounding_box = g.getPolygon(id_bounding_box);
    EXPECT_EQ(g.getPoint(bounding_box.Vertices[0]).X, 0);
    EXPECT_EQ(g.getPoint(bounding_box.Vertices[0]).Y, 1);
    EXPECT_EQ(g.getPoint(bounding_box.Vertices[1]).X, 4);
    EXPECT_EQ(g.getPoint(bounding_box.Vertices[1]).Y, 1);
    EXPECT_EQ(g.getPoint(bounding_box.Vertices[2]).X, 4);
    EXPECT_EQ(g.getPoint(bounding_box.Vertices[2]).Y, 3);
    EXPECT_EQ(g.getPoint(bounding_box.Vertices[3]).X, 0);
    EXPECT_EQ(g.getPoint(bounding_box.Vertices[3]).Y, 3);
}
TEST(TestGeometryLib, TestTraslate){
    GeometryLibrary g;
    int id_trasl_poly;
    Polygon trasl_poly;
    g.reset();
    g.newPoint(4, 1, 0);
    g.newPoint(2, 3, 1);
    g.newPoint(0, 1, 2);
    g.newPolygon({0, 1, 2}, 0);

    id_trasl_poly = g.traslate(g.getPolygon(0), -2, 4);
    trasl_poly = g.getPolygon(id_trasl_poly);
    EXPECT_EQ(g.getPoint(trasl_poly.Vertices[0]).X, 2);
    EXPECT_EQ(g.getPoint(trasl_poly.Vertices[0]).Y, 5);
    EXPECT_EQ(g.getPoint(trasl_poly.Vertices[1]).X, 0);
    EXPECT_EQ(g.getPoint(trasl_poly.Vertices[1]).Y, 7);
    EXPECT_EQ(g.getPoint(trasl_poly.Vertices[2]).X, -2);
    EXPECT_EQ(g.getPoint(trasl_poly.Vertices[2]).Y, 5);
}
}

#endif // __TEST_GEOMLIB_H
