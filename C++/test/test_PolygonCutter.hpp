#ifndef __TEST_POLYCUT_H
#define __TEST_POLYCUT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "PolygonCutter.hpp"

using namespace GeometryLib;
using namespace PolyCut;
using namespace testing;
using namespace std;

namespace CutTesting {

TEST(TestPolygonCutter, TestCutPolygon)
{
    GeometryLibrary g;
    g.reset();
    PolygonCutter cutter = PolygonCutter(g);
    vector<pair<double, double>> points;
    vector<int> polygonVertices;
    tuple<double, double, double, double> segment;
    vector<pair<double, double>> newPoints;
    vector<vector<int>> cuttedPoly;

    points.resize(10);
    polygonVertices.resize(10);

    points[0].first = 2;
    points[0].second = -2;
    polygonVertices[0] = 0;
    points[1].first = 0;
    points[1].second = -1;
    polygonVertices[1] = 1;
    points[2].first = 3;
    points[2].second = 1;
    polygonVertices[2] = 2;
    points[3].first = 0;
    points[3].second = 2;
    polygonVertices[3] = 3;
    points[4].first = 3;
    points[4].second = 2;
    polygonVertices[4] = 4;
    points[5].first = 3;
    points[5].second = 3;
    polygonVertices[5] = 5;
    points[6].first = -1;
    points[6].second = 3;
    polygonVertices[6] = 6;
    points[7].first = -3;
    points[7].second = 1;
    polygonVertices[7] = 7;
    points[8].first = 0;
    points[8].second = 0;
    polygonVertices[8] = 8;
    points[9].first = -3;
    points[9].second = -2;
    polygonVertices[9] = 9;

    for (unsigned int i = 0; i < points.size(); i++){
        g.newPoint(points[i].first, points[i].second, polygonVertices[i]);
    }

    g.newPolygon(polygonVertices, 0);

    g.newPoint(0, -4, g.numberPoints());
    g.newPoint(0, 4, g.numberPoints());
    int s = g.numberSegments();
    g.newSegment(g.numberPoints() - 1, g.numberPoints() - 2, s);

    cuttedPoly = cutter.CutPolygon(g.getPolygon(0), g.getSegment(s));

    cout << "Cut Polyogns" << endl;
    for(auto i = 0; i < cuttedPoly.size(); i++){
        for(auto j = 0; j < cuttedPoly[i].size(); j++){
            cout << cuttedPoly[i][j] << endl;
        }
        cout << "------------" << endl;
    }
}
}

#endif // __TEST_POLYCUT_H
