#ifndef __TEST_MSHMNG_H
#define __TEST_MSHMNG_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "MeshManager.hpp"

using namespace GeometryLib;
using namespace Mesh;
using namespace testing;
using namespace std;

namespace MeshTesting {

TEST(TestMeshManager, Test1)
{
    GeometryLibrary g;
    PolygonCutter cutter = PolygonCutter(g);
    g.reset();
    MeshManager manager = MeshManager(g, cutter);
    ReferenceElement ref;
    vector<int> mesh;
    double areaT = 0;

    g.newPoint(2,0,0);
    g.newPoint(10,1.5,1);
    g.newPoint(10,6,2);
    g.newPoint(8,8,3);
    g.newPoint(6,5,4);
    g.newPoint(0,4,5);

    g.newPoint(0.0, 0.0, 6);
    g.newPoint(35,5,7);
    g.newPoint(41,25,8);
    g.newPoint(7,19,9);

    g.newPolygon({0,1,2,3,4,5}, 0);
    g.newPolygon({6,7,8,9}, 1);

    manager.CreateReferenceElement(g.getPolygon(0), g.getPolygon(g.computeBoundingBox(g.getPolygon(0))), ref);
    manager.CreateMesh(g.getPolygon(1), ref, mesh, false);

    for(unsigned int i = 0; i < ref.Polygons.size(); i++){
        areaT += g.computeArea(g.getPolygon(ref.Polygons[i]));
    }
    EXPECT_LE(abs(areaT - /*130*/80), 1e-7);

    areaT = 0;
    for (unsigned int i = 0; i < mesh.size(); i++){
        areaT += g.computeArea(g.getPolygon(mesh[i]));
    }
    double areaD = g.computeArea(g.getPolygon(1));
    EXPECT_LE(abs(areaT - areaD), 1e-7);
}

TEST(TestMeshManager, Test2){
    GeometryLibrary g;
    PolygonCutter cutter = PolygonCutter(g);
    g.reset();
    MeshManager manager = MeshManager(g, cutter);
    ReferenceElement ref;
    vector<int> mesh;
    double areaT = 0;
    g.newPoint(8, 5, 0);
    g.newPoint(6, 10, 1);
    g.newPoint(0, 3, 2);
    g.newPoint(3, 0, 3);
    g.newPoint(7, 4, 4);
    g.newPoint(9, 0, 5);
    g.newPoint(13, 4, 6);
    g.newPoint(11, 10, 7);

    g.newPoint(-26, -15, 8);
    g.newPoint(18, -8, 9);
    g.newPoint(27, 16, 10);
    g.newPoint(-17, 7, 11);

    g.newPolygon({0,1,2,3,4,5,6,7}, 0);
    g.newPolygon({8,9,10,11}, 1);

    manager.CreateReferenceElement(g.getPolygon(0), g.getPolygon(g.computeBoundingBox(g.getPolygon(0))), ref);
    manager.CreateMesh(g.getPolygon(1), ref, mesh, false);

    for(unsigned int i = 0; i < ref.Polygons.size(); i++){
        areaT += g.computeArea(g.getPolygon(ref.Polygons[i]));
    }
    EXPECT_LE(abs(areaT - 130), 1e-7);

    areaT = 0;
    for (unsigned int i = 0; i < mesh.size(); i++){
        areaT += g.computeArea(g.getPolygon(mesh[i]));
    }
    double areaD = g.computeArea(g.getPolygon(1));
    EXPECT_LE(abs(areaT - areaD), 1e-7);
}

TEST(TestMeshManager, Test3)
{
    GeometryLibrary g;
    PolygonCutter cutter = PolygonCutter(g);
    g.reset();
    MeshManager manager = MeshManager(g, cutter);
    ReferenceElement ref;
    vector<int> mesh;
    double areaT = 0;

    g.newPoint(2,0,0);
    g.newPoint(10,1.5,1);
    g.newPoint(10,6,2);
    g.newPoint(8,8,3);
    g.newPoint(6,5,4);
    g.newPoint(0,4,5);

    g.newPoint(0.0, 0.0, 6);
    g.newPoint(35,5,7);
    g.newPoint(41,25,8);
    g.newPoint(7,19,9);

    g.newPolygon({0,1,2,3,4,5}, 0);
    g.newPolygon({6,7,8,9}, 1);

    int id_bB = g.computeBoundingBox(g.getPolygon(1));

    manager.CreateReferenceElement(g.getPolygon(0), g.getPolygon(g.computeBoundingBox(g.getPolygon(0))), ref);
    manager.CreateMesh(g.getPolygon(1), ref, mesh, true);

    for(unsigned int i = 0; i < ref.Polygons.size(); i++){
        areaT += g.computeArea(g.getPolygon(ref.Polygons[i]));
    }
    EXPECT_LE(abs(areaT - /*130*/80), 1e-7);

    areaT = 0;
    for (unsigned int i = 0; i < mesh.size(); i++){
        areaT += g.computeArea(g.getPolygon(mesh[i]));
    }
    double areaD = g.computeArea(g.getPolygon(id_bB));
    EXPECT_LE(abs(areaT - areaD), 1e-7);
}

TEST(TestMeshManager, Test4){
    GeometryLibrary g;
    PolygonCutter cutter = PolygonCutter(g);
    g.reset();
    MeshManager manager = MeshManager(g, cutter);
    ReferenceElement ref;
    vector<int> mesh;
    double areaT = 0;
    g.newPoint(8, 5, 0);
    g.newPoint(6, 10, 1);
    g.newPoint(0, 3, 2);
    g.newPoint(3, 0, 3);
    g.newPoint(7, 4, 4);
    g.newPoint(9, 0, 5);
    g.newPoint(13, 4, 6);
    g.newPoint(11, 10, 7);

    g.newPoint(-26, -15, 8);
    g.newPoint(18, -8, 9);
    g.newPoint(27, 16, 10);
    g.newPoint(-17, 7, 11);

    g.newPolygon({0,1,2,3,4,5,6,7}, 0);
    g.newPolygon({8,9,10,11}, 1);

    int id_bB = g.computeBoundingBox(g.getPolygon(1));

    manager.CreateReferenceElement(g.getPolygon(0), g.getPolygon(g.computeBoundingBox(g.getPolygon(0))), ref);
    manager.CreateMesh(g.getPolygon(1), ref, mesh, true);

    for(unsigned int i = 0; i < ref.Polygons.size(); i++){
        areaT += g.computeArea(g.getPolygon(ref.Polygons[i]));
    }
    EXPECT_LE(abs(areaT - 130), 1e-7);

    areaT = 0;
    for (unsigned int i = 0; i < mesh.size(); i++){
        areaT += g.computeArea(g.getPolygon(mesh[i]));
    }
    double areaD = g.computeArea(g.getPolygon(id_bB));
    EXPECT_LE(abs(areaT - areaD), 1e-7);
}
}

#endif // __TEST_POLYCUT_H
